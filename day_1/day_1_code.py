results = []

with open("day_1_data.txt") as infile:
    data = infile.read()

for elf_data in data.split("\n\n"):
    results.append(eval(elf_data.replace("\n", "+")))

sorted_results = sorted(results, reverse=True)

print(sorted_results[0])
print(sum(sorted_results[0:3]))